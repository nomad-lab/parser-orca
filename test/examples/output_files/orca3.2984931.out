
                                 *****************
                                 * O   R   C   A *
                                 *****************

           --- An Ab Initio, DFT and Semiempirical electronic structure package ---

                  #######################################################
                  #                        -***-                        #
                  #  Department of molecular theory and spectroscopy    #
                  #              Directorship: Frank Neese              #
                  # Max Planck Institute for Chemical Energy Conversion #
                  #                  D-45470 Muelheim/Ruhr              #
                  #                       Germany                       #
                  #                                                     #
                  #                  All rights reserved                #
                  #                        -***-                        #
                  #######################################################


                         Program Version 3.0.3 - RELEASE   -


 With contributions from (in alphabetic order):
   Ute Becker             : Parallelization
   Dmytro Bykov           : SCF Hessian
   Dmitry Ganyushin       : Spin-Orbit,Spin-Spin,Magnetic field MRCI
   Andreas Hansen         : Spin unrestricted coupled pair/coupled cluster methods
   Dimitrios Liakos       : Extrapolation schemes; parallel MDCI
   Robert Izsak           : Overlap fitted RIJCOSX, COSX-SCS-MP3
   Christian Kollmar      : KDIIS, OOCD, Brueckner-CCSD(T), CCSD density
   Simone Kossmann        : Meta GGA functionals, TD-DFT gradient, OOMP2, MP2 Hessian
   Taras Petrenko         : DFT Hessian,TD-DFT gradient, ASA and ECA modules, normal mode analysis, Resonance Raman, ABS, FL, XAS/XES, NRVS
   Christoph Reimann      : Effective Core Potentials
   Michael Roemelt        : Restricted open shell CIS
   Christoph Riplinger    : Improved optimizer, TS searches, QM/MM, DLPNO-CCSD
   Barbara Sandhoefer     : DKH picture change effects
   Igor Schapiro          : Molecular dynamics
   Kantharuban Sivalingam : CASSCF convergence, NEVPT2
   Boris Wezisla          : Elementary symmetry handling
   Frank Wennmohs         : Technical directorship


 We gratefully acknowledge several colleagues who have allowed us to
 interface, adapt or use parts of their codes:
   Stefan Grimme, W. Hujo, H. Kruse, T. Risthaus : VdW corrections, initial TS optimization,
                                                   DFT functionals, gCP
   Ed Valeev                                     : LibInt (2-el integral package), F12 methods
   Garnet Chan, S. Sharma, R. Olivares           : DMRG
   Ulf Ekstrom                                   : XCFun DFT Library
   Mihaly Kallay                                 : mrcc  (arbitrary order and MRCC methods)
   Andreas Klamt, Michael Diedenhofen            : otool_cosmo (COSMO solvation model)
   Frank Weinhold                                : gennbo (NPA and NBO analysis)
   Christopher J. Cramer and Donald G. Truhlar   : smd solvation model


 Your calculation uses the libint2 library for the computation of 2-el integrals
 For citations please refer to: http://libint.valeyev.net

 This ORCA versions uses:
   CBLAS   interface :  Fast vector & matrix operations
   LAPACKE interface :  Fast linear algebra routines


WARNING: Replacing TZV by TZV-ZORA
Your calculation utilizes the basis: TZV-ZORA
Cite in your paper:
H - Ba, Hf - Hg: D. A. Pantazis, X. Y. Chen, C. R. Landis and F. Neese, J. Chem. Theory Comput. 4, 908 (2008)
La-Lu          : D. A. Pantazis and F. Neese, J. Chem. Theory Comput. 5, 2229 (2009)
Ac-Lr          : D. A. Pantazis and F. Neese, J. Chem. Theory Comput. 7, 677 (2011)
Tl-Rn          : D. A. Pantazis and F. Neese, Theor. Chem. Acc. 131, 1292 (2012)

================================================================================
                                        WARNINGS
                       Please study these warnings very carefully!
================================================================================
Now building the actual basis set


INFO   : the flag for use of LIBINT has been found!

================================================================================
                                       INPUT FILE
================================================================================
NAME = orca.inp
|  1> ! OLYP def2-TZVP(-f) def2-TZVP/J ZORA TightSCF
|  2> #
|  3> %maxcore 1024
|  4> #
|  5> #%tddft	OrbWin[0] = 0,0,-1,-1
|  6> #	NRoots 25
|  7> #	MaxDim 150
|  8> #	DoQuad true
|  9> #	end
| 10> #
| 11> * int 0 1
| 12> Zr 0 0 0 0 0 0
| 13> O 1 2 3 2.15 0 0
| 14> O 1 2 3 2.15 109.4712 0
| 15> O 1 2 3 2.15 109.4712 120
| 16> O 1 2 3 2.15 109.4712 240
| 17> *
| 18> 
| 19>                          ****END OF INPUT****
================================================================================

                       ****************************
                       * Single Point Calculation *
                       ****************************

---------------------------------
CARTESIAN COORDINATES (ANGSTROEM)
---------------------------------
  Zr     0.000000    0.000000    0.000000
  O      2.150000    0.000000    0.000000
  O     -0.716666    2.027040    0.000000
  O     -0.716666   -1.013520   -1.755468
  O     -0.716666   -1.013520    1.755468

----------------------------
CARTESIAN COORDINATES (A.U.)
----------------------------
  NO LB      ZA    FRAG    MASS        X           Y           Z
   0 Zr   40.0000    0    91.220          0.000000000000000          0.000000000000000          0.000000000000000
   1 O     8.0000    0    15.999          4.062911187930691          0.000000000000000          0.000000000000000
   2 O     8.0000    0    15.999         -1.354302349776838          3.830549890864406          0.000000000000000
   3 O     8.0000    0    15.999         -1.354302349776838         -1.915274945432202         -3.317353515952285
   4 O     8.0000    0    15.999         -1.354302349776838         -1.915274945432205          3.317353515952284

--------------------------------
INTERNAL COORDINATES (ANGSTROEM)
--------------------------------
 Zr     0   0   0   0.000000     0.000     0.000
 O      1   2   3   2.150000     0.000     0.000
 O      1   2   3   2.150000   109.471     0.000
 O      1   2   3   2.150000   109.471   120.000
 O      1   2   3   2.150000   109.471   240.000

---------------------------
INTERNAL COORDINATES (A.U.)
---------------------------
 Zr     0   0   0   0.000000     0.000     0.000
 O      1   2   3   4.062911     0.000     0.000
 O      1   2   3   4.062911   109.471     0.000
 O      1   2   3   4.062911   109.471   120.000
 O      1   2   3   4.062911   109.471   240.000

---------------------
BASIS SET INFORMATION
---------------------
There are 2 groups of distinct atoms

 Group   1 Type Zr  : 19s14p9d1f contracted to 12s9p5d1f pattern {811111111111/611111111/51111/1}
 Group   2 Type O   : 11s6p2d contracted to 6s3p2d pattern {611111/411/11}

Atom   0Zr   basis set group =>   1
Atom   1O    basis set group =>   2
Atom   2O    basis set group =>   2
Atom   3O    basis set group =>   2
Atom   4O    basis set group =>   2
-------------------------------
AUXILIARY BASIS SET INFORMATION
-------------------------------
There are 2 groups of distinct atoms

 Group   1 Type Zr  : 26s7p4d3f4g contracted to 26s7p4d3f4g pattern {11111111111111111111111111/1111111/1111/111/1111}
 Group   2 Type O   : 12s5p4d2f1g contracted to 6s4p3d1f1g pattern {711111/2111/211/2/1}

Atom   0Zr   basis set group =>   1
Atom   1O    basis set group =>   2
Atom   2O    basis set group =>   2
Atom   3O    basis set group =>   2
Atom   4O    basis set group =>   2
------------------------------------------------------------------------------
                           ORCA GTO INTEGRAL CALCULATION
                           -- RI-GTO INTEGRALS CHOSEN --
------------------------------------------------------------------------------

                         BASIS SET STATISTICS AND STARTUP INFO

Gaussian basis set:
 # of primitive gaussian shells          ...  119
 # of primitive gaussian functions       ...  269
 # of contracted shells                  ...   71
 # of contracted basis functions         ...  171
 Highest angular momentum                ...    3
 Maximum contraction depth               ...    8
Auxiliary gaussian basis set:
 # of primitive gaussian shells          ...  140
 # of primitive gaussian functions       ...  404
 # of contracted shells                  ...  104
 # of contracted aux-basis functions     ...  320
 Highest angular momentum                ...    4
 Maximum contraction depth               ...    7
Ratio of auxiliary to basis functions    ...  1.87
Integral package used                  ... LIBINT
 One Electron integrals                  ... done
 Ordering auxiliary basis shells         ... done
 Integral threshhold             Thresh  ...  2.500e-11
 Primitive cut-off               TCut    ...  2.500e-12
 Pre-screening matrix                    ... done
 Shell pair data                         ... 
 Ordering of the shell pairs             ... done (   0.000 sec) 2142 of 2556 pairs
 Determination of significant pairs      ... done (   0.000 sec)
 Creation of shell pair data             ... done (   0.000 sec)
 Storage of shell pair data              ... done (   0.003 sec)
 Shell pair data done in (   0.004 sec)
 Computing two index integrals           ... done
 Cholesky decomposition of the V-matrix  ... done


Timings:
 Total evaluation time                   ...   0.252 sec (  0.004 min)
 One electron matrix time                ...   0.027 sec (  0.000 min) = 10.8%
 Schwartz matrix evaluation time         ...   0.122 sec (  0.002 min) = 48.3%
 Two index repulsion integral time       ...   0.006 sec (  0.000 min) =  2.4%
 Cholesky decomposition of V             ...   0.059 sec (  0.001 min) = 23.5%
 Three index repulsion integral time     ...   0.000 sec (  0.000 min) =  0.0%

------------------------------------------------------------------------------
                          ORCA RELATIVISTIC HAMILTONIAN
------------------------------------------------------------------------------

Relativistic Method            ... ZORA(MP)
Treatment of potential         ... Van Wuellen
Nucleus model                  ... Point Nucleus
Speed of light used            ... 137.03598950000000


            *************************************************
            *         ZERO ORDER REGULAR APPROXIMATION      *
            *                     ZORA(MP)                  *
            *            Programmed by FN according to      *
            *                   C. van Wuellen              *
            *       J. Chem. Phys. (1998) vol 109, 392-399  *
            *************************************************

Terms included in model potential: +V(e,N)+V(C)+V(Xa)+V(VWN-V)
Basis function cutoff value   ... 1.000000e-11
Loading the fitted atomic densities (ZORA)   ... done
Calculating the cutoffs                      ... done
Number of fit functions                      ... 72
Setting up the integration grid ... done
-------------------------------------------------------------------------------
                                 ORCA SCF
-------------------------------------------------------------------------------

------------
SCF SETTINGS
------------
Hamiltonian:
 Density Functional     Method          .... DFT(GTOs)
 Exchange Functional    Exchange        .... OPTX
 Correlation Functional Correlation     .... LYP
 Gradients option       PostSCFGGA      .... off
 RI-approximation to the Coulomb term is turned on
   Number of auxiliary basis functions  .... 320

Relativistic Settings:
 Scalar relativistic method             .... ZORA
 Speed of light used       Velit        ....       137.035989
Orbital energies will be scaled


General Settings:
 Integral files         IntName         .... orca
 Hartree-Fock type      HFTyp           .... RHF
 Total Charge           Charge          ....    0
 Multiplicity           Mult            ....    1
 Number of Electrons    NEL             ....   72
 Basis Dimension        Dim             ....  171
 Nuclear Repulsion      ENuc            ....    372.9225043875 Eh

Convergence Acceleration:
 DIIS                   CNVDIIS         .... on
   Start iteration      DIISMaxIt       ....    12
   Startup error        DIISStart       ....  0.200000
   # of expansion vecs  DIISMaxEq       ....     5
   Bias factor          DIISBfac        ....   1.050
   Max. coefficient     DIISMaxC        ....  10.000
 Newton-Raphson         CNVNR           .... off
 SOSCF                  CNVSOSCF        .... off
 Level Shifting         CNVShift        .... on
   Level shift para.    LevelShift      ....    0.2500
   Turn off err/grad.   ShiftErr        ....    0.0010
 Zerner damping         CNVZerner       .... off
 Static damping         CNVDamp         .... on
   Fraction old density DampFac         ....    0.7000
   Max. Damping (<1)    DampMax         ....    0.9800
   Min. Damping (>=0)   DampMin         ....    0.0000
   Turn off err/grad.   DampErr         ....    0.1000
 Fernandez-Rico         CNVRico         .... off

SCF Procedure:
 Maximum # iterations   MaxIter         ....   125
 SCF integral mode      SCFMode         .... Direct
   Integral package                     .... LIBINT
 Reset frequeny         DirectResetFreq ....    20
 Integral Threshold     Thresh          ....  2.500e-11 Eh
 Primitive CutOff       TCut            ....  2.500e-12 Eh

Convergence Tolerance:
 Convergence Check Mode ConvCheckMode   .... Total+1el-Energy
 Energy Change          TolE            ....  1.000e-08 Eh
 1-El. energy change                    ....  1.000e-05 Eh
 DIIS Error             TolErr          ....  5.000e-07


Diagonalization of the overlap matrix:
Smallest eigenvalue                        ... 2.808e-04
Time for diagonalization                   ...    0.020 sec
Threshold for overlap eigenvalues          ... 1.000e-08
Number of eigenvalues below threshold      ... 0
Time for construction of square roots      ...    0.007 sec
Total time needed                          ...    0.027 sec

-------------------
DFT GRID GENERATION
-------------------

General Integration Accuracy     IntAcc      ...  4.340
Radial Grid Type                 RadialGrid  ... Gauss-Chebyshev
Angular Grid (max. acc.)         AngularGrid ... Lebedev-110
Angular grid pruning method      GridPruning ... 3 (G Style)
Weight generation scheme         WeightScheme... Becke
Basis function cutoff            BFCut       ...    1.0000e-11
Integration weight cutoff        WCut        ...    1.0000e-14
Grids for H and He will be reduced by one unit

# of grid points (after initial pruning)     ...  10144 (   0.0 sec)
# of grid points (after weights+screening)   ...  10006 (   0.0 sec)
nearest neighbour list constructed           ...    0.0 sec
Grid point re-assignment to atoms done       ...    0.0 sec
Grid point division into batches done        ...    0.0 sec
Reduced shell lists constructed in    0.1 sec

Total number of grid points                  ...    10006
Total number of batches                      ...      159
Average number of points per batch           ...       62
Average number of grid points per atom       ...     2001
Average number of shells per batch           ...    36.33 (51.16%)
Average number of basis functions per batch  ...    97.36 (56.94%)
Average number of large shells per batch     ...    28.12 (77.41%)
Average number of large basis fcns per batch ...    75.66 (77.71%)
Maximum spatial batch extension              ...  29.88, 39.26, 23.08 au
Average spatial batch extension              ...   5.40,  5.28,  4.77 au

Time for grid setup =    0.114 sec

------------------------------
INITIAL GUESS: MODEL POTENTIAL
------------------------------
Loading ZORA relativistic densities                ... done
Calculating cut-offs                               ... done
Setting up the integral package                    ... done
Initializing the effective Hamiltonian             ... done
Starting the Coulomb interaction                   ... done (   0.1 sec)
Reading the grid                                   ... done
Mapping shells                                     ... done
Starting the XC term evaluation                    ... done (   0.2 sec)
  promolecular density results
     # of electrons  =     72.000172418
     EX              =   -148.141758333
     EC              =     -3.106057759
     EX+EC           =   -151.247816093
Transforming the Hamiltonian                       ... done (   0.0 sec)
Diagonalizing the Hamiltonian                      ... done (   0.0 sec)
Back transforming the eigenvectors                 ... done (   0.0 sec)
Now organizing SCF variables                       ... done
                      ------------------
                      INITIAL GUESS DONE (   0.5 sec)
                      ------------------
--------------
SCF ITERATIONS
--------------
ITER       Energy         Delta-E        Max-DP      RMS-DP      [F,P]     Damp
               ***  Starting incremental Fock matrix formation  ***
  0  -3932.2017152556   0.000000000000 0.12075498  0.00307369  0.7597795 0.7000
  1  -3932.3270218596  -0.125306603999 0.02190722  0.00082797  0.5201401 0.7000
  2  -3932.3760033966  -0.048981536959 0.02702801  0.00079985  0.3585371 0.7000
  3  -3932.4237258154  -0.047722418868 0.01858830  0.00042829  0.2476127 0.7000
                               ***Turning on DIIS***
  4  -3932.4560055362  -0.032279720766 0.02178634  0.00042179  0.1707362 0.7000
  5  -3932.4764857242  -0.020480188007 0.01277249  0.00026858  0.1175236 0.7000
  6  -3932.4890144020  -0.012528677777 0.01596155  0.00058065  0.0806831 0.0000
  7  -3932.5252928743  -0.036278472314 0.00750699  0.00017061  0.0197383 0.0000
  8  -3932.5266607363  -0.001367862038 0.00438596  0.00008390  0.0053802 0.0000
  9  -3932.5266916217  -0.000030885343 0.00273014  0.00004832  0.0070088 0.0000
 10  -3932.5267775353  -0.000085913638 0.00078528  0.00002246  0.0024868 0.0000
 11  -3932.5267861469  -0.000008611587 0.00037215  0.00001343  0.0021254 0.0000
 12  -3932.5267868227  -0.000000675832 0.00037553  0.00000891  0.0021161 0.0000
 13  -3932.5267912259  -0.000004403144 0.00202461  0.00004228  0.0018482 0.0000
 14  -3932.5267539562   0.000037269718 0.00054682  0.00001166  0.0037779 0.0000
 15  -3932.5267682688  -0.000014312671 0.00012306  0.00000481  0.0029799 0.0000
 16  -3932.5267738819  -0.000005613089 0.00006674  0.00000289  0.0028724 0.0000
 17  -3932.5267773508  -0.000003468846 0.00005899  0.00000229  0.0027381 0.0000
 18  -3932.5267805419  -0.000003191154 0.00314227  0.00005366  0.0025638 0.0000
 19  -3932.5267289326   0.000051609302 0.00153236  0.00002765  0.0055013 0.0000
               *** Restarting incremental Fock matrix formation ***
                                   *** Resetting DIIS ***
 20  -3932.5267293787  -0.000000446075 0.00777363  0.00017235  0.0044158 0.0000
 21  -3932.5255282373   0.001201141391 0.00142587  0.00004607  0.0179183 0.0000
 22  -3932.5259571274  -0.000428890102 0.00052508  0.00001921  0.0145476 0.0000
 23  -3932.5261392030  -0.000182075595 0.00038696  0.00001129  0.0129771 0.0000
 24  -3932.5262572506  -0.000118047598 0.00037827  0.00000869  0.0118257 0.0000
 25  -3932.5263479408  -0.000090690179 0.00635638  0.00011760  0.0108405 0.0000
 26  -3932.5267181590  -0.000370218216 0.00364306  0.00006531  0.0060112 0.0000
 27  -3932.5266447504   0.000073408556 0.00105708  0.00002228  0.0094068 0.0000
 28  -3932.5266813958  -0.000036645376 0.00042635  0.00001152  0.0067714 0.0000
 29  -3932.5266872788  -0.000005883035 0.00025567  0.00000727  0.0063613 0.0000
 30  -3932.5266945367  -0.000007257887 0.00683965  0.00012935  0.0059631 0.0000
 31  -3932.5261989278   0.000495608926 0.00213979  0.00005358  0.0149488 0.0000
 32  -3932.5263039116  -0.000104983790 0.00105602  0.00002651  0.0114254 0.0000
 33  -3932.5263630447  -0.000059133143 0.00071126  0.00001535  0.0110962 0.0000
 34  -3932.5264250144  -0.000061969618 0.00046898  0.00000969  0.0108689 0.0000
 35  -3932.5264749523  -0.000049937933 0.01552527  0.00024372  0.0103321 0.0000
 36  -3932.5249502997   0.001524652555 0.00349364  0.00007311  0.0302546 0.0000
 37  -3932.5254244596  -0.000474159893 0.00150807  0.00003914  0.0220944 0.0000
 38  -3932.5255946999  -0.000170240303 0.00054847  0.00001954  0.0193721 0.0000
 39  -3932.5257210806  -0.000126380626 0.00038953  0.00001154  0.0177992 0.0000
               *** Restarting incremental Fock matrix formation ***
                                   *** Resetting DIIS ***
 40  -3932.5258199487  -0.000098868117 0.03092665  0.00061118  0.0168434 0.0000
 41  -3932.5108674907   0.014952457975 0.00569343  0.00016846  0.0655577 0.0000
 42  -3932.5156032456  -0.004735754919 0.00197087  0.00007048  0.0551354 0.0000
 43  -3932.5174251235  -0.001821877901 0.00101421  0.00003853  0.0510200 0.0000
 44  -3932.5185487076  -0.001123584057 0.00085618  0.00002630  0.0481635 0.0000
 45  -3932.5194080100  -0.000859302447 0.04138716  0.00068120  0.0457629 0.0000
 46  -3932.5194018963   0.000006113764 0.01749651  0.00031117  0.0569506 0.0000
 47  -3932.5181066712   0.001295225064 0.00443466  0.00010151  0.0506058 0.0000
 48  -3932.5191353888  -0.001028717604 0.00154307  0.00005245  0.0443528 0.0000
 49  -3932.5195255795  -0.000390190743 0.00298642  0.00006011  0.0414779 0.0000
 50  -3932.5214153820  -0.001889802440 0.04072828  0.00077127  0.0375914 0.0000
 51  -3932.5119340161   0.009481365930 0.00711863  0.00018873  0.0665557 0.0000
 52  -3932.5142763558  -0.002342339728 0.00238817  0.00008544  0.0574297 0.0000
 53  -3932.5151825732  -0.000906217451 0.00129975  0.00004405  0.0532954 0.0000
 54  -3932.5158476040  -0.000665030800 0.00068181  0.00002698  0.0512637 0.0000
 55  -3932.5164218730  -0.000574268988 0.08488726  0.00151035  0.0500860 0.0000
 56  -3932.4455937201   0.070828152965 0.00675145  0.00016279  0.1639616 0.7000
 57  -3932.4526715043  -0.007077784271 0.00809245  0.00019245  0.1480934 0.7000
 58  -3932.4585033823  -0.005831878013 0.00697069  0.00016808  0.1310502 0.7000
 59  -3932.4626031493  -0.004099766928 0.00499745  0.00013131  0.1229104 0.7000
               *** Restarting incremental Fock matrix formation ***
                                   *** Resetting DIIS ***
 60  -3932.4660767994  -0.003473650143 0.05724748  0.00138249  0.1255899 0.7000
 61  -3932.4967538153  -0.030677015866 0.13066115  0.00257821  0.0582668 0.0000
 62  -3932.0309463153   0.465807499975 0.00446731  0.00015858  0.2859135 0.7000
 63  -3932.0607172145  -0.029770899158 0.00505992  0.00018461  0.2759134 0.7000
 64  -3932.0922045647  -0.031487350190 0.00438298  0.00017353  0.2665351 0.7000
 65  -3932.1188249718  -0.026620407112 0.00419725  0.00015202  0.2597582 0.7000
 66  -3932.1276518310  -0.008826859283 0.15022300  0.00260776  0.2608550 0.7000
 67  -3932.4373437571  -0.309691926065 0.23890307  0.00491112  0.0654270 0.0000
 68  -3931.2419785982   1.195365158888 0.01389134  0.00039288  0.4936731 0.7000
 69  -3931.3194165339  -0.077437935695 0.01406256  0.00045420  0.4675846 0.7000
 70  -3931.3951932980  -0.075776764078 0.01077179  0.00040680  0.4394803 0.7000
 71  -3931.4563593190  -0.061166021035 0.00908816  0.00035424  0.4147700 0.7000
 72  -3931.4970586995  -0.040699380449 0.14065171  0.00488074  0.4153870 0.7000
 73  -3931.8528874213  -0.355828721852 0.05494484  0.00235860  0.2159442 0.7000
 74  -3931.4435148111   0.409372610228 0.04338278  0.00166671  0.3401786 0.7000
 75  -3931.2769318661   0.166582945046 0.03627251  0.00130208  0.3393422 0.7000
 76  -3931.1735966103   0.103335255781 0.02814308  0.00101535  0.4021168 0.7000
 77  -3931.1102248938   0.063371716476 0.02732495  0.00109662  0.4394216 0.7000
 78  -3931.1115532480  -0.001328354221 0.13030179  0.00322087  0.4552727 0.7000
 79  -3932.0527837983  -0.941230550258 0.09797142  0.00238429  0.2105946 0.7000
               *** Restarting incremental Fock matrix formation ***
                                   *** Resetting DIIS ***
 80  -3932.1727340572  -0.119950258893 0.12158170  0.00272309  0.2434651 0.7000
 81  -3932.1834687245  -0.010734667327 0.06961512  0.00169466  0.1728955 0.7000
 82  -3931.9664853359   0.216983388563 0.04164592  0.00122184  0.3443136 0.7000
 83  -3931.9731700367  -0.006684700716 0.03589798  0.00100523  0.3378066 0.7000
 84  -3932.0759826539  -0.102812617205 0.02833365  0.00086170  0.2605007 0.7000
 85  -3932.1776521627  -0.101669508793 0.04803326  0.00100792  0.2065690 0.7000
 86  -3932.2687040892  -0.091051926529 0.06384710  0.00146910  0.2434141 0.7000
 87  -3932.3606490544  -0.091944965212 0.04254566  0.00127595  0.1547550 0.7000
 88  -3932.3256713145   0.034977739852 0.03788460  0.00111442  0.2481541 0.7000
 89  -3932.2010874703   0.124583844208 0.03817756  0.00097262  0.2838391 0.7000
 90  -3932.0259016073   0.175185863081 0.04066837  0.00093363  0.2804922 0.7000
 91  -3931.9150621926   0.110839414614 0.04985386  0.00117861  0.3095283 0.7000
 92  -3931.9896408884  -0.074578695810 0.06026434  0.00127104  0.3909437 0.7000
 93  -3932.0706971825  -0.081056294046 0.05361402  0.00127666  0.3912238 0.7000
 94  -3932.0489257986   0.021771383917 0.06000386  0.00169317  0.3230075 0.7000
 95  -3932.1481848874  -0.099259088865 0.04820542  0.00121110  0.3225796 0.7000
 96  -3932.2192831937  -0.071098306295 0.03765228  0.00103257  0.2511534 0.7000
 97  -3932.2888080231  -0.069524829380 0.04156180  0.00098303  0.2449513 0.7000
 98  -3932.3223944291  -0.033586406031 0.03970881  0.00090365  0.1876587 0.7000
 99  -3932.3239220724  -0.001527643239 0.02730352  0.00086350  0.2034610 0.7000
               *** Restarting incremental Fock matrix formation ***
                                   *** Resetting DIIS ***
100  -3932.3605864864  -0.036664414020 0.08670884  0.00206541  0.1750024 0.7000
101  -3932.4464957078  -0.085909221354 0.13694513  0.00369160  0.0894011 0.0000
102  -3931.5143984739   0.932097233888 0.01196128  0.00030787  0.4516293 0.7000
103  -3931.5945346066  -0.080136132717 0.01944866  0.00044442  0.4390211 0.7000
104  -3931.6737847976  -0.079250190989 0.02381318  0.00050560  0.4284247 0.7000
105  -3931.7172227574  -0.043437959801 0.03094215  0.00061997  0.4206027 0.7000
106  -3931.6857305339   0.031492223505 0.13282275  0.00448906  0.4220651 0.7000
107  -3932.0295435097  -0.343812975824 0.07379960  0.00215698  0.1660591 0.7000
108  -3931.5874209728   0.442122536892 0.03670527  0.00150441  0.3009315 0.7000
109  -3931.3622864899   0.225134482938 0.03172214  0.00123763  0.3351224 0.7000
110  -3931.3075774915   0.054708998338 0.02370359  0.00107395  0.4094874 0.7000
111  -3931.3306087371  -0.023031245524 0.02050385  0.00098499  0.4668910 0.7000
112  -3931.3978413313  -0.067232594256 0.11910155  0.00303305  0.5007099 0.7000
113  -3932.2130064316  -0.815165100317 0.09166483  0.00213584  0.2135235 0.7000
114  -3932.2591331253  -0.046126693645 0.06082683  0.00139881  0.2036606 0.7000
115  -3932.0913547459   0.167778379415 0.04187992  0.00104800  0.2501558 0.7000
116  -3931.9183489205   0.173005825404 0.02970645  0.00087407  0.3021848 0.7000
117  -3931.7822922873   0.136056633111 0.03185187  0.00109885  0.3741661 0.7000
118  -3931.6928147438   0.089477543540 0.03122332  0.00102900  0.4468831 0.7000
119  -3931.7030054658  -0.010190722014 0.07314096  0.00191098  0.4555775 0.7000
               *** Restarting incremental Fock matrix formation ***
                                   *** Resetting DIIS ***
120  -3932.0774783742  -0.374472908401 0.14700872  0.00274351  0.2901466 0.7000
121  -3932.2512210547  -0.173742680458 0.08931442  0.00219234  0.2126269 0.7000
122  -3932.2331553398   0.018065714824 0.04378760  0.00141681  0.2555224 0.7000
123  -3932.0620397820   0.171115557877 0.03837818  0.00112660  0.3190417 0.7000
124  -3931.9673125774   0.094727204556 0.03953909  0.00097684  0.3443760 0.7000

               *****************************************************
               *                      ERROR                        *
               *        SCF NOT CONVERGED AFTER 125 CYCLES         *
               *****************************************************


---------------
SCF CONVERGENCE
---------------

  Last Energy change         ...    9.4727e-02  Tolerance :   1.0000e-08
  Last MAX-Density change    ...    1.8219e-02  Tolerance :   1.0000e-07
  Last RMS-Density change    ...    5.3323e-04  Tolerance :   5.0000e-09
  Last DIIS Error            ...    3.4438e-01  Tolerance :   5.0000e-07

-------
TIMINGS
-------

Total SCF time: 0 days 0 hours 1 min 1 sec 

Total time                  ....      61.236 sec
Sum of individual times     ....      59.846 sec  ( 97.7%)

Fock matrix formation       ....      56.586 sec  ( 92.4%)
  Coulomb formation         ....      35.975 sec  ( 63.6% of F)
  Split-RI-J                ....      31.978 sec  ( 56.5% of F)
  XC integration            ....      19.736 sec  ( 34.9% of F)
    Basis function eval.    ....       4.015 sec  ( 20.3% of XC)
    Density eval.           ....       3.570 sec  ( 18.1% of XC)
    XC-Functional eval.     ....       0.870 sec  (  4.4% of XC)
    XC-Potential eval.      ....       7.819 sec  ( 39.6% of XC)
Diagonalization             ....       1.750 sec  (  2.9%)
Density matrix formation    ....       0.064 sec  (  0.1%)
Population analysis         ....       0.000 sec  (  0.0%)
Initial guess               ....       0.478 sec  (  0.8%)
Orbital Transformation      ....       0.000 sec  (  0.0%)
Orbital Orthonormalization  ....       0.000 sec  (  0.0%)
DIIS solution               ....       0.854 sec  (  1.4%)
Grid generation             ....       0.114 sec  (  0.2%)

-------------------------   --------------------
FINAL SINGLE POINT ENERGY         0.000000000000
-------------------------   --------------------


                            ***************************************
                            *     ORCA property calculations      *
                            ***************************************

                                    ---------------------
                                    Active property flags
                                    ---------------------
   (+) Dipole Moment


------------------------------------------------------------------------------
                       ORCA ELECTRIC PROPERTIES CALCULATION
------------------------------------------------------------------------------

Dipole Moment Calculation                       ... on
Quadrupole Moment Calculation                   ... off
Polarizability Calculation                      ... off
GBWName                                         ... orca.gbw
Electron density file                           ... orca.scfp.tmp
Warning (ORCA_ELPROP): failed to read the density matrix:orca.scfp.tmp of Dimension 171

Timings for individual modules:

Sum of individual times         ...       62.620 sec (=   1.044 min)
GTO integral calculation        ...        0.355 sec (=   0.006 min)   0.6 %
Relativistic integrals          ...        0.440 sec (=   0.007 min)   0.7 %
SCF iterations                  ...       61.825 sec (=   1.030 min)  98.7 %
                             ****ORCA TERMINATED NORMALLY****
TOTAL RUN TIME: 0 days 0 hours 1 minutes 2 seconds 828 msec


------------------------------------------------------
Statistics for LoadLeveler job ll1-ib0.2984931.0:

number of nodes: 1
number of MPI tasks: 8
number of OpenMP threads per task: 1

memory consumption (high water mark): 0.14 GB

average CPU usage: 10.4 %

